<?php

class CoursesController extends PHPRest\Abstracts\ControllerAbstract{

  private $_controller_vars;
  private $_parameters;
  private $_view_format;

	public function __construct( $these_controller_vars, $these_parameters, $this_view_format ){

    $this->_controller_vars = array();
    if( isset( $these_controller_vars ) ){
      $this->_controller_vars = $these_controller_vars;
    }
    $this->_parameters = array();
    if( isset( $these_parameters ) ){
      $this->_parameters = $these_parameters;
    }
    $this->_view_format = $this_view_format;
	}

}
