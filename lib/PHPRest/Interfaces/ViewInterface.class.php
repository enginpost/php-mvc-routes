<?php

namespace PHPRest\Interfaces;

interface ViewInterface{
	public function render( $data ); // A view render function
}
