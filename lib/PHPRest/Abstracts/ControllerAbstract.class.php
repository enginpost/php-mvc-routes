<?php

namespace PHPRest\Abstracts;

use PHPRest\Interfaces as Interfaces;

abstract class ControllerAbstract implements Interfaces\ControllerInterface{

	public function __construct( $these_controller_vars, $these_parameters, $this_view_format ){}

}
